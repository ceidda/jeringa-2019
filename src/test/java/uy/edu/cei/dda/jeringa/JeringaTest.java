package uy.edu.cei.dda.jeringa;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

public class JeringaTest {

	@Test
	public void test() throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		PersonService personService = new PersonService();
		Jeringa.inject(personService);
		assertNotNull("get poison null", personService.getPoison());
		assertTrue(personService.getPoison() instanceof Cobra);
	}

}
