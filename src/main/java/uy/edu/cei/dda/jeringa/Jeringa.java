package uy.edu.cei.dda.jeringa;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Jeringa {

	private static final Logger logger = LoggerFactory.getLogger(Jeringa.class);

	public static void inject(Object object) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> clazz = object.getClass();
		logger.debug("processing class: {}", clazz);
		Field[] fields = clazz.getDeclaredFields();
		for(Field field : fields) {
			logger.debug("field found for class {} with name {}", clazz, field.getName());
			Annotation[] annotations = field.getAnnotations();
			for(Annotation annotation : annotations) {
				logger.debug("annotation found for field {} with type {}", field.getName(), annotation);
				if(annotation instanceof JeringaAguja) {
					JeringaAguja jeringaAguja = (JeringaAguja) annotation;
					String value = jeringaAguja.value();
					logger.debug("annotation with value {}", value);
					Class<?> newClazz = Class.forName(value);
					Constructor<?> constructor = newClazz.getConstructor();
					Object newObject = constructor.newInstance();
					
					field.setAccessible(true);
					field.set(object, newObject);
					field.setAccessible(false);
				}
			}
		}
	}

}
